import axios from 'axios'

export default class ChemblAPI {
  delayedJobsAPI() {
    const path = process.env.delayedJobsApiUrl
    return axios.create({
      baseURL: path,
      withCredentials: false,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache'
      }
    })
  }
  // resultFileAPI() {
  //   const path = 'http://hx-rke-wp-webadmin-04-worker-3.caas.ebi.ac.uk:30316/'
  // }
}
