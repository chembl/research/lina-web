import ChemblAPI from '@/api/chemblAPI'

const delayedJobsAPI = new ChemblAPI().delayedJobsAPI()

export const state = () => ({
  jobID: '',
  loading: true,
  jobStatusResponse: '',
  isPredicting: false,
  resultURLs: {},
  progress: 0,
  gotFiles: false,
  isQueued: false,
  isRunning: false,
  error: {},
  expiryDate: '',
  historyData: ''
})

export const mutations = {
  SET_LOADING(state, flag) {
    state.loading = flag
  },
  SET_JOB_ID(state, jobResponse) {
    state.jobID = jobResponse.job_id
  },
  SET_JOB_STATUS_RESPONSE(state, jobStatusResponse) {
    state.jobStatusResponse = jobStatusResponse
    state.progress = jobStatusResponse.progress
    if (jobStatusResponse.status === 'FINISHED') {
      state.resultURLs = jobStatusResponse.output_files_urls
      state.isPredicting = false
      if (state.resultURLs['hist_data.json'] && state.resultURLs['predictions.csv']) {
        state.gotFiles = true
        state.historyData = state.resultURLs['hist_data.json']
      } else {
        state.error = new Error('No results files hist_data.json or prediction.csv')
      }
      state.isRunning = false
      state.isQueued = false
    } else if (jobStatusResponse.status === 'QUEUED') {
      state.gotFiles = false
      state.isRunning = false
      state.isQueued = true
    } else if (jobStatusResponse.status === 'RUNNING') {
      state.isQueued = false
      state.isRunning = true
    } else if (jobStatusResponse.status === 'ERROR') {
      state.isPredicting = false
      state.isRunning = false
      state.isQueued = false
      state.gotFiles = false
      state.error = new Error('Job Error: Please try again later')
    }
  },
  SET_IS_PREDICTING(state, isPredicting) {
    state.isPredicting = isPredicting
  },
  SET_ERROR(state, error) {
    state.error = error
  },
  RESET_STATE(state) {
    state.isPredicting = false
    state.isRunning = false
    state.isQueued = false
    state.gotFiles = false
    state.progress = 0
  },
  setExpiryDate: state => {
    const date = new Date()
    date.setHours(date.getHours() + 72)
    state.expiryDate = date
  }
}

export const actions = {
  async postDataset({ commit }, query) {
    commit('SET_LOADING', true)
    commit('SET_ERROR', {})
    commit('RESET_STATE')
    commit('setExpiryDate')

    const formData = new FormData()
    const config = {
      headers: {
        'content-type': `multipart/form-data; boundary=${formData._boundary}`
      }
    }
    formData.append('input1', query.file)
    formData.append('standardise', query.standardise)
    formData.append('dl__ignore_cache', query.ignoreCache)

    await delayedJobsAPI
      .post('/submit/mmv_job', formData, config)
      .then(res => {
        if (res.status === 200) {
          commit('SET_JOB_ID', res.data)
          commit('SET_IS_PREDICTING', true)
        } else {
          commit('SET_ERROR', new Error(res))
          commit('SET_IS_PREDICTING', false)
        }
        commit('SET_LOADING', false)
      })
      .catch(error => {
        console.log(error)
        commit('SET_ERROR', error)
        commit('SET_LOADING', false)
        commit('SET_IS_PREDICTING', false)
      })
  },
  async getJobStatus({ commit }, query) {
    await delayedJobsAPI
      .get(`/status/${query.jobID}`)
      .then(res => {
        if (res.status === 200) {
          commit('SET_JOB_STATUS_RESPONSE', res.data)
        } else {
          commit('SET_ERROR', new Error(res))
          commit('SET_IS_PREDICTING', false)
        }
      })
      .catch(error => {
        console.log(error)
        commit('SET_ERROR', error)
        commit('SET_LOADING', false)
        commit('SET_IS_PREDICTING', false)
      })
  }
}
