import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

export default ctx => {
  const vuetify = new Vuetify({
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: '#00BFA5',
          accent: '#09979B',
          secondary: '#75D8D5',
          success: '#4CAF50',
          info: '#2196F3',
          warning: '#FB8C00',
          error: '#FF5252',
          background: '#2e2e2e'
        },
        light: {
          primary: '#09979B',
          accent: '#00BFA5',
          secondary: '#0D595F',
          success: '#4CAF50',
          info: '#2196F3',
          warning: '#FB8C00',
          error: '#FF5252'
        }
      }
    }
  })

  ctx.app.vuetify = vuetify
  ctx.$vuetify = vuetify.framework
}
