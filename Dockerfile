FROM node:lts
ENV ENV_FILE_PATH=${ENV_FILE_PATH:-'/etc/run_config/.env'}

LABEL maintainer="heinzke@ebi.ac.uk"

ENV APP_SOURCE /usr/src/malaria

RUN mkdir -p $APP_SOURCE
WORKDIR $APP_SOURCE

RUN apt-get update -qq -y && \
    apt-get upgrade -qq -y && \
    apt-get install -qq -y git

COPY . $APP_SOURCE

ARG DELAYED_JOBS_API
ENV DELAYED_JOBS_API $DELAYED_JOBS_API

RUN npm install

ENV HOST 0.0.0.0

EXPOSE 3000

ENTRYPOINT ENV_FILE_PATH=${ENV_FILE_PATH} npm run build && npm start
